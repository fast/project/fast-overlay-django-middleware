# FAST Overlay Middleware

Easy middleware to include the FAST overlay in your python3+ Django projects!

Once you've set up your project at [https://fast.uwaterloo.ca](https://fast.uwaterloo.ca), grab the ID from the url, and add the following to your `settings.py`:


```python
MIDDLEWARE.append('fast_overlay_middleware.middleware.OverlayMiddleware')

FAST_PROJECT_ID = <id of your project>
```

After which all TemplateResponses (anything generated by render()) will automatically have the FAST overlay attached to it!
