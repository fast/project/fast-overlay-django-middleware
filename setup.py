import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="fast-overlay-middleware",
    version="1",
    author="Mirko Vucicevich",
    author_email="mvucicev@uwaterloo.ca",
    description="Middleware for embedding the FAST overlay",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=['fast_overlay_middleware'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
