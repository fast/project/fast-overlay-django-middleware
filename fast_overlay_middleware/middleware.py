from django.conf import settings
from django.template.response import TemplateResponse
from django.http import HttpResponse


def OverlayMiddleware(get_response):
    """Injects the FAST overlay onto any TemplateResponse"""

    fast_id = getattr(settings, 'FAST_PROJECT_ID', None)
    if fast_id is None:
        raise Exception('IMPROPERLY CONFIGURED: Please set FAST_OVERLAY_ID')

    fast_script_url = getattr(
        settings,
        'FAST_SCRIPT_URL',
        'https://fast.uwaterloo.ca/static/fast-overlay.js'
    )

    script = (
        '<script src="{}"></script>\n'
        '<script>window.projector.mount({{ id: {} }})</script>\n'
        '</body>'
    ).format(fast_script_url, fast_id).encode('UTF-8')

    def middleware(request):
        response = get_response(request)
        if isinstance(response, (TemplateResponse, HttpResponse)):
            response.content = response.content.replace(b'</body>', script)
        return response

    return middleware
